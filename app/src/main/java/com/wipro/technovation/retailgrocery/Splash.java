package com.wipro.technovation.retailgrocery;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import java.io.File;


public class Splash extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        //for status bar color
        Window window = Splash.this.getWindow();

        // clear FLAG_TRANSLUCENT_STATUS flag:
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                // finally change the color
                window.setStatusBarColor(Splash.this.getResources().getColor(R.color.colorPrimary));
            }

        }

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 2000ms
                if (Build.VERSION.SDK_INT >= 23) {
                    checkForPermissions();
                } else {
                    Log.d(getString(R.string.debug_tag), "Explicit permissions not needed");
                    startMainActivity();
                }
            }
        }, 2500);
    }

    private void checkForPermissions() {
        if (ContextCompat.checkSelfPermission(Splash.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(Splash.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(Splash.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d(getString(R.string.debug_tag), "Explicit permission not granted!");
            ActivityCompat.requestPermissions(Splash.this,
                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST);
        } else {

            startMainActivity();

        }
    }

    private void startMainActivity() {
//        Intent service = new Intent(Splash.this, BluetoothService.class);
//        if (!isMyServiceRunning(BluetoothService.class, Splash.this)) {
//            service.setAction(STARTFOREGROUND_ACTION);
//            Splash.this.startService(service);
//            Log.d(getString(R.string.debug_tag), "Bluetooth service started from splash screen");
//        }

        startActivity(new Intent(Splash.this, RangingActivity.class));
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.d(getString(R.string.debug_tag), "Permission Granted");

            File retailFileRoot = new File(Environment.getExternalStorageDirectory(), "Retail Demo");
            if (!retailFileRoot.exists()) {
                retailFileRoot.mkdirs();
            }

            File retailApparelFileRoot = new File(Environment.getExternalStorageDirectory(), "Retail Demo/Retail Grocery");
            if (!retailApparelFileRoot.exists()) {
                retailApparelFileRoot.mkdirs();
            }

            startMainActivity();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        ((BeaconReferenceApplication) this.getApplicationContext()).setMonitoringActivity(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        ((BeaconReferenceApplication) this.getApplicationContext()).setMonitoringActivity(null);
    }

//    public static boolean isMyServiceRunning(Class<?> serviceClass, Context myContext) {
//        ActivityManager manager = (ActivityManager) myContext.getSystemService(Context.ACTIVITY_SERVICE);
//        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
//            if (serviceClass.getName().equals(service.service.getClassName())) {
//                return true;
//            }
//        }
//        return false;
//    }

}
