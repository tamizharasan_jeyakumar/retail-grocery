package com.wipro.technovation.retailgrocery;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import java.io.File;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private Button btPlan;

    private SharedPreferences sharedPrefs;
    private SharedPreferences.Editor editor;
    private String filename;

    private String[] exploreImages = {"explore1.png", "explore2.png", "explore3.png"};
    private String[] discoverImages = {"discover1.png", "discover2.png", "discover3.png"};
    private String[] saveImages = {"save1.png", "save2.png", "save3.png"};

    private String[] storeExploreImages = {"store_explore1.png", "store_explore2.png", "store_explore3.png"};
    private String[] storeDiscoverImages = {"store_discover1.png", "store_discover2.png", "store_discover3.png"};
    private String[] storeSaveImages = {"store_save1.png", "store_save2.png", "store_save3.png"};

    //All assets related to Apparel App
    final File retailGroceryFileRoot = new File(Environment.getExternalStorageDirectory(), "Retail Demo/Retail Grocery");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        editor = sharedPrefs.edit();

        btPlan = (Button) findViewById(R.id.btplan);


        if (sharedPrefs.getBoolean("store", false)) {
            btPlan.setText("Start shopping");
        }
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, GroceryList.class));
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Explore Carousal
        CarouselView carouselViewExploren = (CarouselView) findViewById(R.id.cvExplore);
        carouselViewExploren.setPageCount(exploreImages.length);
        carouselViewExploren.setImageListener(new ImageListener() {

            @Override
            public void setImageForPosition(int position, final ImageView imageView) {
                if (sharedPrefs.getBoolean("store", false)) {
                    filename = retailGroceryFileRoot.getAbsolutePath() + "/" + storeExploreImages[position];
                    imageView.setTag(storeExploreImages[position]);
                } else {
                    filename = retailGroceryFileRoot.getAbsolutePath() + "/" + exploreImages[position];
                    imageView.setTag(exploreImages[position]);
                }
                //Log.d(getString(R.string.debug_tag), "File name: " + filename);
                Bitmap bitmap = BitmapFactory.decodeFile(filename);
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                imageView.setImageBitmap(bitmap);

                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d(getString(R.string.debug_tag), "Image Tag: " + imageView.getTag());
                    }
                });
            }
        });

        //Discover Carousal
        CarouselView carouselViewDiscover = (CarouselView) findViewById(R.id.cvDiscover);
        carouselViewDiscover.setPageCount(discoverImages.length);
        carouselViewDiscover.setImageListener(new ImageListener() {

            @Override
            public void setImageForPosition(int position, final ImageView imageView) {
                if (sharedPrefs.getBoolean("store", false)) {
                    filename = retailGroceryFileRoot.getAbsolutePath() + "/" + storeDiscoverImages[position];
                    imageView.setTag(storeDiscoverImages[position]);
                } else {
                    filename = retailGroceryFileRoot.getAbsolutePath() + "/" + discoverImages[position];
                    imageView.setTag(discoverImages[position]);
                }
                //Log.d(getString(R.string.debug_tag), "File name: " + filename);
                Bitmap bitmap = BitmapFactory.decodeFile(filename);
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                imageView.setImageBitmap(bitmap);
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d(getString(R.string.debug_tag), "Image Tag: " + imageView.getTag());
                        if (imageView.getTag().toString().contains("discover2"))
                            startActivity(new Intent(MainActivity.this, ProductCatalog.class).putExtra("product", "discover2_product"));
                    }
                });
            }
        });

        //Save Carousal
        CarouselView carouselViewSave = (CarouselView) findViewById(R.id.cvSave);
        carouselViewSave.setPageCount(saveImages.length);
        carouselViewSave.setImageListener(new ImageListener() {

            @Override
            public void setImageForPosition(int position, final ImageView imageView) {
                if (sharedPrefs.getBoolean("store", false)) {
                    filename = retailGroceryFileRoot.getAbsolutePath() + "/" + storeSaveImages[position];
                    imageView.setTag(storeSaveImages[position]);
                } else {
                    filename = retailGroceryFileRoot.getAbsolutePath() + "/" + saveImages[position];
                    imageView.setTag(saveImages[position]);
                }
                //Log.d(getString(R.string.debug_tag), "File name: " + filename);
                Bitmap bitmap = BitmapFactory.decodeFile(filename);
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                imageView.setImageBitmap(bitmap);
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d(getString(R.string.debug_tag), "Image Tag: " + imageView.getTag());
                        if (imageView.getTag().toString().contains("save2"))
                            startActivity(new Intent(MainActivity.this, ProductCatalog.class).putExtra("product", "save2_product"));
                        else if (imageView.getTag().toString().contains("save3"))
                            startActivity(new Intent(MainActivity.this, ProductCatalog.class).putExtra("product", "save3_product"));
                    }
                });
            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            exitApp();
            //super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(MainActivity.this, UserSettingActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.list)
            startActivity(new Intent(MainActivity.this, Search.class));
        else if (id == R.id.nav_store) {
            editor.putBoolean("store", true);
            editor.apply();
            startActivity(new Intent(MainActivity.this, MainActivity.class));
            finish();
        }

//        if (id == R.id.nav_camera) {
//            // Handle the camera action
//        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {
//
//        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void showSearchScreen(View v) {
        if (sharedPrefs.getBoolean("store", false)) {
            startActivity(new Intent(MainActivity.this, GroceryList.class));
        } else {
            startActivity(new Intent(MainActivity.this, Search.class));
        }
    }

    public void exitApp() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setCancelable(false);
        builder.setTitle("Exiting App?");
        builder.setMessage("Are you sure you want to exit app?").setPositiveButton("Exit App", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.remove("mycart");
                editor.remove("store");
                //editor.clear();
                editor.apply();

                String myCartListString = sharedPrefs.getString("mycart", "");
                Log.d(getString(R.string.debug_tag), "Size of prefs: " + myCartListString.length());
                //finish();
                Intent intent = new Intent(getApplicationContext(), RangingActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("EXIT", true);
                startActivity(intent);
                finish();

            }
        }).setNegativeButton("Nope", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

}
