package com.wipro.technovation.retailgrocery;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class ProductCatalog extends AppCompatActivity {

    private CartData myCartData;
    private ArrayList<CartData> myCartDataList;
    private String product;
    private String filename;
    private Bitmap bitmap;

    private SharedPreferences sharedPrefs;

    private Animation fab_shake;

    private FloatingActionButton fab;

    private ImageView ivProduct1, ivProduct2, ivProduct3, ivProduct4, ivProduct5;

    //All assets related to Apparel App
    final File retailGroceryFileRoot = new File(Environment.getExternalStorageDirectory(), "Retail Demo/Retail Grocery");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_catalog);

        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        ivProduct1 = (ImageView) findViewById(R.id.ivProduct1);
        ivProduct2 = (ImageView) findViewById(R.id.ivProduct2);
        ivProduct3 = (ImageView) findViewById(R.id.ivProduct3);
        ivProduct4 = (ImageView) findViewById(R.id.ivProduct4);
        ivProduct5 = (ImageView) findViewById(R.id.ivProduct5);

        String poistion = getIntent().getStringExtra("product");
        Log.d(getString(R.string.debug_tag), "Product name: " + poistion);

        switch (poistion) {
            case "discover2_product":
                product = "chewy";
                break;

            case "save2_product":
                product = "biglow";
                break;

            case "save3_product":
                product = "ghira";
                break;
        }

        filename = retailGroceryFileRoot.getAbsolutePath() + "/" + product + "1.png";
        Log.d(getString(R.string.debug_tag), "File name: " + filename);
        bitmap = BitmapFactory.decodeFile(filename);
        ivProduct1.setScaleType(ImageView.ScaleType.FIT_XY);
        ivProduct1.setImageBitmap(bitmap);

        filename = retailGroceryFileRoot.getAbsolutePath() + "/" + product + "2.png";
        Log.d(getString(R.string.debug_tag), "File name: " + filename);
        bitmap = BitmapFactory.decodeFile(filename);
        ivProduct2.setScaleType(ImageView.ScaleType.FIT_XY);
        ivProduct2.setImageBitmap(bitmap);

        filename = retailGroceryFileRoot.getAbsolutePath() + "/" + product + "3.png";
        Log.d(getString(R.string.debug_tag), "File name: " + filename);
        bitmap = BitmapFactory.decodeFile(filename);
        ivProduct3.setScaleType(ImageView.ScaleType.FIT_XY);
        ivProduct3.setImageBitmap(bitmap);

        filename = retailGroceryFileRoot.getAbsolutePath() + "/" + product + "4.png";
        Log.d(getString(R.string.debug_tag), "File name: " + filename);
        bitmap = BitmapFactory.decodeFile(filename);
        ivProduct4.setScaleType(ImageView.ScaleType.FIT_XY);
        ivProduct4.setImageBitmap(bitmap);

        filename = retailGroceryFileRoot.getAbsolutePath() + "/" + product + "5.png";
        Log.d(getString(R.string.debug_tag), "File name: " + filename);
        bitmap = BitmapFactory.decodeFile(filename);
        ivProduct5.setScaleType(ImageView.ScaleType.FIT_XY);
        ivProduct5.setImageBitmap(bitmap);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProductCatalog.this, GroceryList.class));
                finish();
            }
        });
    }

    public void addItemToCart(View v) {

        Toast.makeText(ProductCatalog.this, "Item Added to cart", Toast.LENGTH_SHORT).show();
        fab_shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
        fab.startAnimation(fab_shake);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                fab.clearAnimation();
            }
        }, 2500);

        myCartData = new CartData();
        if (product.contains("biglow")) {
            myCartData.setItemNumber(5);
            myCartData.setItemType(2);
            myCartData.setImageName("bigelow.png");
            myCartData.setItemName("Bigelow Tea variety Pack, 8 flavour");
            myCartData.setWeight("10 oz");
            myCartData.setOldPrice("$ 25.40");
            myCartData.setActualPrice("$ 24.40");
            myCartData.setRackNumber("A1 B2");
            myCartData.setQty(1);
        } else if (product.contains("ghira")) {
            myCartData.setItemNumber(7);
            myCartData.setItemType(5);
            myCartData.setImageName("chocolate.png");
            myCartData.setItemName("Ghirardelli Chocolate Intense Dark Bar");
            myCartData.setWeight("30 oz");
            myCartData.setOldPrice("$ 20");
            myCartData.setActualPrice("$ 15.22");
            myCartData.setRackNumber("A2 B2");
            myCartData.setQty(1);
        } else if (product.contains("chewy")) {
            myCartData.setItemNumber(6);
            myCartData.setItemType(3);
            myCartData.setImageName("chewy.png");
            myCartData.setItemName("Quaker Chewy Granola Bars - 18 bars");
            myCartData.setWeight("10 oz");
            myCartData.setOldPrice("$ 4.5");
            myCartData.setActualPrice("$ 3.5");
            myCartData.setRackNumber("C1 C3");
            myCartData.setQty(1);
        }

        //Shared preferences

        Gson gson = new Gson();
        String myCartListString = sharedPrefs.getString("mycart", "");
        Type type = new TypeToken<ArrayList<CartData>>() {
        }.getType();


        if (myCartListString.length() > 1) {
            myCartDataList = gson.fromJson(myCartListString, type);
            Log.d(getString(R.string.debug_tag), "Cart found in shared prefs");

        } else {
            myCartDataList = new ArrayList<>();
        }
        myCartDataList.add(myCartData);
        saveCartList();

    }

    private void saveCartList() {
        SharedPreferences.Editor editor = sharedPrefs.edit();
        Gson gson = new Gson();

        String myCartListString = gson.toJson(myCartDataList);
        editor.putString("mycart", myCartListString);
        editor.apply();
        Log.d(getString(R.string.debug_tag), "cart saved in memory " + "Length: " + myCartDataList.size());
    }
}
