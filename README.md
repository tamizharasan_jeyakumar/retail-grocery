# README #

Usage Instructions:

	This app is very close to a beta version of the production app. Meaning, there are some configuration files (JSON files) which can be used to configure the entire app as per your story board.
	The assets contain there JSON files namely cart.json, recommend.json & replace.json. 
	The cart.json is responsible for populating the cart with default items once the Genie button is clicked. If the user wishes to change the items then they may do so by just changing the parameters like image name, product name etc. Each product MUST be assigned with an index (an identifier) and the default product index is shared below on the ‘Product Index Table’ section
	You can add ‘N’ number of items in the cart.json to auto populate a list of items when Genie is engaged
	Any item in the cart can be replaced with an ‘Alternate’ product or a ‘Substitute’ product. The configuration of this replacement can be handled via the recommend.json & replace.json files
	Example, if say the Tropicana juice in the cart is about to get ‘Altered’. So in the recommend.json, the index value MUST match the default index of the product
	Then the option (‘opt’) must be ‘1’ meaning Alternate. Then the JSON keys of 1, 2 & 3 can have any picture asset name which will be present at Retail Grocery folder as mentioned above in the phone memory directory structure 
	Users can typically choose up to three alternate products and the key of 1, 2 & 3 will have the alternate product detail image. If user wants to restrict the number of Alternative suggestions then by putting ‘X’ would be sufficient against each key
	Similarly, if the user wants to provide ‘substitute’ suggestions then in recommend.json the option (‘opt’) must be ‘2’ meaning the ‘substitute’. Here as well user will get three slots to display images and if user wants to restrict the number of substitute suggestions then putting ‘X’ would be sufficient against each key
	Now in order to replace an existing product with an alternate/substitute product we use the replace.json file
	In the file we use the image name as the primary index. So in the previous step if an alternate/substitute product was displayed and the same image is clicked then we use the ‘Image name’ as index and we supply all the item related information such as type, cart image, name etc. to be populated in the cart 


Product Index Table:

Index	Product Name
1		Tropicana Juice
2		Yogi Tea
3		Kellogg’s Corn Flakes
4		Diet Coke
5		Bigelow Tea variety Pack
6		Quaker Chewy Granola Bars
7		Ghirardelli Chocolate Intense Dark Bar

Product Item type code:

Type Code	Type
1			Juice
2			Tea
3			Breakfast
4			Cool Drinks
5			Chocolates



### What is this repository for? ###

This repo is for capturing the code related to retail grocery app

### How do I get set up? ###

	Copy & place the APK in the device internal memory and click on the APK file for installing
	If Android phone doesn’t allow installation please make sure the setting in our Android phone under Security-> Unknown Sources is tuned ON
	Post installation the App can be used like just another Android app
	After the first time the app auto creates the folder structure as mentioned above in device internal memory 
	Connect the phone to a PC/Laptop via USB cable & just paste the picture assets in the Retail Apparel folder as if to a pen drive and you are good to go
	Please make sure while replacing images the name, image resolution, image Type (PNF) are maintained as is for optimal performance 

### Contribution guidelines ###

Tamil Jayakumar

### Who do I talk to? ###

tamizharasan.jayakumar@wipro.com