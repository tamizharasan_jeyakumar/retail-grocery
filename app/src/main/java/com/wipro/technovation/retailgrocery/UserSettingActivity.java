package com.wipro.technovation.retailgrocery;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * Created by TJAYA on 1/5/2017.
 */

public class UserSettingActivity extends PreferenceActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.settings);

    }
}
