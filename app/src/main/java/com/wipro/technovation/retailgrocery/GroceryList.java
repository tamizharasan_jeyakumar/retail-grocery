package com.wipro.technovation.retailgrocery;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class GroceryList extends AppCompatActivity implements CartAdapter.cartListCallback {

    //All assets related to Apparel App
    final File retailApparelFileRoot = new File(Environment.getExternalStorageDirectory(), "Retail Demo/Retail Grocery");

    private CartData myCartData;
    private ArrayList<CartData> myCartDataList;
    private CartAdapter myCardAdapter;

    //Shared preferences
    private SharedPreferences sharedPrefs;

    private int[] cartOrder = {5, 6, 3, 1, 4, 7};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grocery_list);


        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        final SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                myCardAdapter.notifyDataSetChanged();
                Toast.makeText(GroceryList.this, "Refresh success", Toast.LENGTH_LONG).show();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            Gson gson = new Gson();
            String myCartListString = sharedPrefs.getString("mycart", "");
            Type type = new TypeToken<ArrayList<CartData>>() {
            }.getType();

            if (myCartListString.length() > 1) {
                myCartDataList = gson.fromJson(myCartListString, type);
                Log.d(getString(R.string.debug_tag), "Cart found in shared prefs");
            } else {
                myCartDataList = new ArrayList<>();
                myCartData = new CartData();
                String cartJson = getJsonFromFile();
                JSONArray cartArray = new JSONArray(cartJson);
                for (int i = 0; i < cartArray.length(); i++) {
                    JSONObject obj = cartArray.getJSONObject(i);
                    myCartData = new CartData();
                    myCartData.setItemNumber(obj.getInt("index"));
                    myCartData.setItemType(obj.getInt("type"));
                    myCartData.setImageName(obj.getString("image"));
                    myCartData.setItemName(obj.getString("name"));
                    myCartData.setWeight(obj.getString("weight"));
                    myCartData.setOldPrice(obj.getString("old_price"));
                    myCartData.setActualPrice(obj.getString("price"));
                    myCartData.setRackNumber(obj.getString("rack"));
                    myCartData.setChecked(false);
                    myCartData.setQty(1);
                    myCartDataList.add(myCartData);
                    //saveCartList();
                }
            }

            if (sharedPrefs.getBoolean("store", false)) {
                //getSupportActionBar().setTitle("Hello world App");
                GroceryList.this.setTitle("My Shopping Cart");
                ArrayList<CartData> sortedCartList = new ArrayList<>();
                for (int i = 0; i < cartOrder.length; i++) {
                    for (int j = 0; j < myCartDataList.size(); j++) {
                        if (cartOrder[i] == myCartDataList.get(j).getItemNumber())
                            sortedCartList.add(myCartDataList.get(j));
                    }
                }
                myCartDataList = new ArrayList<>();
                for (int i = 0; i < sortedCartList.size(); i++) {
                    myCartDataList.add(sortedCartList.get(i));
                }
            }

            saveCartList();
            myCardAdapter = new CartAdapter(GroceryList.this, myCartDataList);
            ListView listView = (ListView) findViewById(R.id.listView);
            listView.setAdapter(myCardAdapter);


        } catch (JSONException e1) {
            e1.printStackTrace();
        }

    }

    private void saveCartList() {
        SharedPreferences.Editor editor = sharedPrefs.edit();
        Gson gson = new Gson();

        String myCartListString = gson.toJson(myCartDataList);
        editor.putString("mycart", myCartListString);
        editor.apply();
        Log.d(getString(R.string.debug_tag), "cart saved in memory " + "Length: " + myCartDataList.size());
    }

    private String getJsonFromFile() {
        FileInputStream inputStream = null;  // 2nd line
        try {
            inputStream = new FileInputStream(new File(retailApparelFileRoot.getAbsolutePath() + "/cart.json"));

            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String receiveString = "";
            StringBuilder stringBuilder = new StringBuilder();

            while ((receiveString = bufferedReader.readLine()) != null) {
                stringBuilder.append(receiveString);
            }

            inputStream.close();
            return (stringBuilder.toString());
//                String cartJson = stringBuilder.toString();
//                JSONArray cartArray = new JSONArray(cartJson);
//                Log.d(getString(R.string.debug_tag), "Json File: " + cartJson);
//                Log.d(getString(R.string.debug_tag), "Json Size: " + cartArray.length());

        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public void removeItem(int position) {
        Log.d(getString(R.string.debug_tag), "Position is: " + position);
        myCartDataList.remove(position);
        saveCartList();
        myCardAdapter.notifyDataSetChanged();
        Toast.makeText(this, "Item removed from cart!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void updateQty(int position, int qty) {
        myCartDataList.get(position).setQty(qty);
        saveCartList();
    }

    @Override
    public void checkBoxSort(int position, int index, boolean checked) {
        Log.d(getString(R.string.debug_tag), "Position is: " + position +
                "index: " + index + "checked: " + checked);
        for (int i = 0; i < myCartDataList.size(); i++) {
            if (myCartDataList.get(i).getItemNumber() == index) {
                Log.d(getString(R.string.debug_tag), "updating check at:" + index);
                myCartDataList.get(i).setChecked(checked);
                break;
            }
        }
        saveCartList();


    }
}


