package com.wipro.technovation.retailgrocery;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;

import java.util.Collection;

public class RangingActivity extends AppCompatActivity implements BeaconConsumer {

    private BeaconManager beaconManager = BeaconManager.getInstanceForApplication(this);

    private SharedPreferences sharedPrefs;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranging);

        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(RangingActivity.this);
        editor = sharedPrefs.edit();

        beaconManager.bind(this);
        if (getIntent().getBooleanExtra("EXIT", false)) {
            finish();
        } else {
            startActivity(new Intent(RangingActivity.this, MainActivity.class));
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        beaconManager.unbind(RangingActivity.this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (beaconManager.isBound(this)) beaconManager.setBackgroundMode(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (beaconManager.isBound(this)) beaconManager.setBackgroundMode(false);
    }

    @Override
    public void onBeaconServiceConnect() {
        beaconManager.setRangeNotifier(new RangeNotifier() {
            @Override
            public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
                float range = 1.0f;
                String pref_Range = sharedPrefs.getString("etRange", "1");
                if (pref_Range.length() > 0)
                    range = Float.parseFloat(pref_Range);

                if (beacons.size() > 0) {
                    for (Beacon firstBeacon : beacons) {
                        //Log.d(getString(R.string.debug_tag), "Region address: " + region.getBluetoothAddress() + " UID:" + region.getUniqueId());
                        Log.d(getString(R.string.debug_tag), "The beacon " +
                                firstBeacon.getBluetoothAddress() + " is about " + firstBeacon.getDistance() + " meters away.");
                        if (firstBeacon.getBluetoothAddress().equals(sharedPrefs.getString("etAddress", "00")) &&
                                firstBeacon.getDistance() <= range && !sharedPrefs.getBoolean("store", false)) {
                            sendNotification();
                            editor.putBoolean("store", true);
                            editor.apply();
                            Log.d(getString(R.string.debug_tag), "Addr: " + sharedPrefs.getString("etAddress", "00") + " Range: " + firstBeacon.getDistance());
                            startActivity(new Intent(RangingActivity.this, MainActivity.class));
                            break;
                        }
                    }


                }
            }

        });

        try {
            beaconManager.startRangingBeaconsInRegion(new Region("myRangingUniqueId", null, null, null));
        } catch (RemoteException e) {
        }
    }

    private void sendNotification() {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setContentTitle("Welcome to WiproInsight Store")
                        .setContentText("We have some interesting products just for you!")
                        .setAutoCancel(true)
                        .setDefaults(Notification.DEFAULT_SOUND)
                        .setSmallIcon(R.drawable.grocery_icon);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntent(new Intent(this, MainActivity.class));
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        builder.setContentIntent(resultPendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, builder.build());
    }
}
