package com.wipro.technovation.retailgrocery;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class Recommendation extends AppCompatActivity {

    private ImageView iv1, iv2, iv3;

    private SharedPreferences sharedPrefs;

    private ArrayList<CartData> myCartDataList;

    //All assets related to Apparel App
    final File retailGroceryFileRoot = new File(Environment.getExternalStorageDirectory(), "Retail Demo/Retail Grocery");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recommendation);

        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        iv1 = (ImageView) findViewById(R.id.iv1);
        iv2 = (ImageView) findViewById(R.id.iv2);
        iv3 = (ImageView) findViewById(R.id.iv3);

        iv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getReplacement(iv1.getTag().toString());
            }
        });

        iv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getReplacement(iv2.getTag().toString());
            }
        });

        iv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getReplacement(iv3.getTag().toString());
            }
        });

        Log.d(getString(R.string.debug_tag), "Position: " + getIntent().getIntExtra("position", 0) +
                "Opton: " + getIntent().getIntExtra("opt", 0));


        Gson gson = new Gson();
        String myCartListString = sharedPrefs.getString("mycart", "");
        Type type = new TypeToken<ArrayList<CartData>>() {
        }.getType();

        if (myCartListString.length() > 1) {
            myCartDataList = gson.fromJson(myCartListString, type);
            Log.d(getString(R.string.debug_tag), "Cart found in shared prefs");
        }


        String cartJson = getJsonFromFile("recommend.json");
        try {
            JSONArray recomArray = new JSONArray(cartJson);
            for (int i = 0; i < recomArray.length(); i++) {
                JSONObject obj = recomArray.getJSONObject(i);
                if (getIntent().getIntExtra("index", 0) == obj.getInt("index") &&
                        getIntent().getIntExtra("opt", 0) == obj.getInt("opt")) {
                    if (obj.getString("1").equals("X")) {
                        iv1.setVisibility(View.INVISIBLE);
                        iv1.setClickable(false);
                    } else {
                        setIv1(obj.getString("1"));
                        iv1.setClickable(true);
                        iv1.setTag(obj.getString("1"));
                    }

                    if (obj.getString("2").equals("X")) {
                        iv2.setVisibility(View.INVISIBLE);
                        iv2.setClickable(false);
                    } else {
                        setIv2(obj.getString("2"));
                        iv2.setClickable(true);
                        iv2.setTag(obj.getString("2"));
                    }

                    if (obj.getString("3").equals("X")) {
                        iv3.setVisibility(View.INVISIBLE);
                        iv3.setClickable(false);
                    } else {
                        setIv2(obj.getString("3"));
                        iv3.setClickable(true);
                        iv3.setTag(obj.getString("3"));
                    }
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getReplacement(String tag) {
        Log.d(getString(R.string.debug_tag), "Tag is: " + tag);
        String replJson = getJsonFromFile("replace.json");
        try {
            JSONArray replArray = new JSONArray(replJson);
            for (int i = 0; i < replArray.length(); i++) {
                JSONObject obj = replArray.getJSONObject(i);
                Log.d(getString(R.string.debug_tag), "index is: " + obj.getString("index"));
                if (obj.getString("index").contains(tag)) {
                    CartData myCartData = new CartData();
                    myCartData.setItemNumber(getIntent().getIntExtra("index", 0));
                    myCartData.setItemType(obj.getInt("type"));
                    myCartData.setImageName(obj.getString("image"));
                    myCartData.setItemName(obj.getString("name"));
                    myCartData.setWeight(obj.getString("weight"));
                    myCartData.setOldPrice(obj.getString("old_price"));
                    myCartData.setActualPrice(obj.getString("price"));
                    myCartData.setRackNumber(obj.getString("rack"));
                    myCartData.setQty(1);
                    myCartDataList.set(getIntent().getIntExtra("position", 0), myCartData);
                    saveCartList();
                    Toast.makeText(this, "Cart has been updated successfully", Toast.LENGTH_SHORT).show();
                    Recommendation.this.finish();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setIv1(String fileName) {
        String filename = retailGroceryFileRoot.getAbsolutePath() + "/" + fileName;
        Log.d(getString(R.string.debug_tag), "File name: " + filename);
        Bitmap bitmap = BitmapFactory.decodeFile(filename);
        iv1.setImageBitmap(bitmap);
    }

    private void setIv2(String fileName) {
        String filename = retailGroceryFileRoot.getAbsolutePath() + "/" + fileName;
        Log.d(getString(R.string.debug_tag), "File name: " + filename);
        Bitmap bitmap = BitmapFactory.decodeFile(filename);
        iv2.setImageBitmap(bitmap);
    }

    private void setIv3(String fileName) {
        String filename = retailGroceryFileRoot.getAbsolutePath() + "/" + fileName;
        Log.d(getString(R.string.debug_tag), "File name: " + filename);
        Bitmap bitmap = BitmapFactory.decodeFile(filename);
        iv3.setImageBitmap(bitmap);
    }


    private String getJsonFromFile(String fileName) {
        FileInputStream inputStream = null;  // 2nd line
        try {
            inputStream = new FileInputStream(new File(retailGroceryFileRoot.getAbsolutePath() + "/" + fileName));

            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String receiveString = "";
            StringBuilder stringBuilder = new StringBuilder();

            while ((receiveString = bufferedReader.readLine()) != null) {
                stringBuilder.append(receiveString);
            }

            inputStream.close();
            Log.d(getString(R.string.debug_tag), "Json File: " + stringBuilder.toString());
            return (stringBuilder.toString());
//                String cartJson = stringBuilder.toString();
//                JSONArray cartArray = new JSONArray(cartJson);

//                Log.d(getString(R.string.debug_tag), "Json Size: " + cartArray.length());

        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    private void saveCartList() {
        SharedPreferences.Editor editor = sharedPrefs.edit();
        Gson gson = new Gson();

        String myCartListString = gson.toJson(myCartDataList);
        editor.putString("mycart", myCartListString);
        editor.apply();
        Log.d(getString(R.string.debug_tag), "cart saved in memory " + "Length: " + myCartDataList.size());
    }
}
