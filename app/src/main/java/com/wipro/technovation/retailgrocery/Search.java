package com.wipro.technovation.retailgrocery;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class Search extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
    }

    public void showDialog(View v) {
        final ProgressDialog progress = new ProgressDialog(this);
        progress.setTitle("Calling ..Genie...");
        progress.setMessage("Please wait while Genie is out of the bottle..");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 2000ms
                progress.dismiss();
                startActivity(new Intent(Search.this, GroceryList.class));
                finish();
            }
        }, 3500);

    }
}
