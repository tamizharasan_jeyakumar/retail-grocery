package com.wipro.technovation.retailgrocery;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.tubb.smrv.SwipeHorizontalMenuLayout;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by TJAYA on 12/31/2016.
 */

public class CartAdapter extends ArrayAdapter<CartData> {

    private Context context;
    private ArrayList<CartData> cartData;

    //callback
    private cartListCallback myCallback;

    private SharedPreferences sharedPrefs;

    //All assets related to Apparel App
    final File retailGroceryFileRoot = new File(Environment.getExternalStorageDirectory(), "Retail Demo/Retail Grocery");


    // View lookup cache
    private static class ViewHolder {
        TextView tvItemName, tvItemWeight, tvItemOldPrice, tvPrice, tvQty, tvRackName;
        ImageView ivCategory, ivPlus, ivMinus, ivProductImage;
        CheckBox cbSelect;
        SwipeHorizontalMenuLayout sml;
        View btLeft;
        View btAlternate, btSubstitute;

    }


    public CartAdapter(GroceryList groceryList, ArrayList<CartData> myCartDataList) {
        super(groceryList, R.layout.cart, myCartDataList);
        this.context = groceryList;
        this.cartData = myCartDataList;
        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final CartData mCartData = cartData.get(position);
        //Log.d("groceryTag", "Cart data ready for population: " + mCartData.getItemName());

        this.myCallback = (cartListCallback) parent.getContext();

        // Check if an existing view is being reused, otherwise inflate the view
        final ViewHolder viewHolder; // view lookup cache stored in tag
        final View result;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.cart, parent, false);

            viewHolder.tvItemName = (TextView) convertView.findViewById(R.id.tvItemName);
            viewHolder.tvItemWeight = (TextView) convertView.findViewById(R.id.tvItemWeight);
            viewHolder.tvItemOldPrice = (TextView) convertView.findViewById(R.id.tvItemOldPrice);
            viewHolder.tvPrice = (TextView) convertView.findViewById(R.id.tvPrice);
            viewHolder.tvQty = (TextView) convertView.findViewById(R.id.tvQty);


            viewHolder.cbSelect = (CheckBox) convertView.findViewById(R.id.cbSelect);

            final View finalConvertView = convertView;
            viewHolder.cbSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (myCallback != null) {
                        myCallback.checkBoxSort(position, cartData.get(position).getItemNumber(), b);
                    }

                    if (b) {
                        viewHolder.ivCategory.setImageAlpha(100);
                        viewHolder.ivProductImage.setImageAlpha(100);
                        finalConvertView.setBackgroundColor(Color.parseColor("#bbbebf"));

                    } else {
                        viewHolder.ivCategory.setImageAlpha(255);
                        viewHolder.ivProductImage.setImageAlpha(255);
                        finalConvertView.setBackgroundColor(Color.parseColor("#00000000"));

                    }
                }
            });

            viewHolder.sml = (SwipeHorizontalMenuLayout) convertView.findViewById(R.id.sml);
            viewHolder.btLeft = convertView.findViewById(R.id.btLeft);
            viewHolder.btLeft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    viewHolder.sml.smoothCloseMenu();
                    if (myCallback != null) {
                        myCallback.removeItem(position);
                    }


                }
            });

            viewHolder.btAlternate = convertView.findViewById(R.id.btAlternate);
            viewHolder.btAlternate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    viewHolder.sml.smoothCloseMenu();
                    Log.d("groceryTag", "Position for Alternate: " + position + "item code: " + cartData.get(position).getItemNumber());
                    context.startActivity(new Intent(context, Recommendation.class).putExtra("opt", 1)
                            .putExtra("index", cartData.get(position).getItemNumber())
                            .putExtra("position", position));
                }
            });

            viewHolder.btSubstitute = convertView.findViewById(R.id.btSubstitute);
            viewHolder.btSubstitute.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    viewHolder.sml.smoothCloseMenu();
                    Log.d("groceryTag", "Position for Substitute: " + position + "item code: " + cartData.get(position).getItemNumber());
                    context.startActivity(new Intent(context, Recommendation.class).putExtra("opt", 2)
                            .putExtra("index", cartData.get(position).getItemNumber())
                            .putExtra("position", position));

                }
            });

            viewHolder.ivCategory = (ImageView) convertView.findViewById(R.id.ivCategory);
            viewHolder.ivPlus = (ImageView) convertView.findViewById(R.id.ivPlus);
            viewHolder.ivMinus = (ImageView) convertView.findViewById(R.id.ivMinus);
            viewHolder.ivProductImage = (ImageView) convertView.findViewById(R.id.ivProductImage);

            viewHolder.ivPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int value = Integer.parseInt(viewHolder.tvQty.getText().toString());
                    if (value < 20)
                        value++;
                    viewHolder.tvQty.setText(Integer.toString(value));
                    if (myCallback != null) {
                        myCallback.updateQty(position, value);
                    }

                }
            });

            viewHolder.ivMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int value = Integer.parseInt(viewHolder.tvQty.getText().toString());
                    if (value > 1)
                        value--;
                    viewHolder.tvQty.setText(Integer.toString(value));
                    if (myCallback != null) {
                        myCallback.updateQty(position, value);
                    }

                }
            });

            result = convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        //
        viewHolder.tvItemName.setText(mCartData.getItemName());
        viewHolder.tvItemWeight.setText(mCartData.getWeight());
        viewHolder.tvItemOldPrice.setText(mCartData.getOldPrice());
        viewHolder.tvItemOldPrice.setPaintFlags(viewHolder.tvItemOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        viewHolder.tvPrice.setText(mCartData.getActualPrice());
        viewHolder.tvQty.setText(Integer.toString(mCartData.getQty()));

        viewHolder.tvRackName = (TextView) convertView.findViewById(R.id.tvRackName);
        if (sharedPrefs.getBoolean("store", false)) {
            viewHolder.tvRackName.setVisibility(View.VISIBLE);
            viewHolder.tvRackName.setText(mCartData.getRackNumber());

            viewHolder.cbSelect.setVisibility(View.VISIBLE);
            viewHolder.cbSelect.setChecked(mCartData.isChecked());

        } else {
            viewHolder.tvRackName.setVisibility(View.INVISIBLE);
            viewHolder.cbSelect.setVisibility(View.INVISIBLE);
        }

        if (mCartData.isChecked()) {
            viewHolder.ivCategory.setImageAlpha(100);
            viewHolder.ivProductImage.setImageAlpha(100);
            convertView.setBackgroundColor(Color.parseColor("#bbbebf"));

        } else {
            viewHolder.ivCategory.setImageAlpha(255);
            viewHolder.ivProductImage.setImageAlpha(255);
            convertView.setBackgroundColor(Color.parseColor("#00000000"));

        }


        String filename = retailGroceryFileRoot.getAbsolutePath() + "/" + mCartData.getImageName();
        //Log.d(getString(R.string.debug_tag), "File name: " + filename);
        Bitmap bitmap = BitmapFactory.decodeFile(filename);
        viewHolder.ivProductImage.setScaleType(ImageView.ScaleType.FIT_XY);
        viewHolder.ivProductImage.setImageBitmap(bitmap);

        String categoryImageName = "";
        switch (mCartData.getItemType()) {

            case 1:
                categoryImageName = retailGroceryFileRoot.getAbsolutePath() + "/juice.png";
                break;

            case 2:
                categoryImageName = retailGroceryFileRoot.getAbsolutePath() + "/tea.png";
                break;

            case 3:
                categoryImageName = retailGroceryFileRoot.getAbsolutePath() + "/breakfast.png";
                break;

            case 4:
                categoryImageName = retailGroceryFileRoot.getAbsolutePath() + "/coke.png";
                break;

            case 5:
                categoryImageName = retailGroceryFileRoot.getAbsolutePath() + "/chocolates.png";
                break;

        }

        Bitmap categortBitmap = BitmapFactory.decodeFile(categoryImageName);
        viewHolder.ivCategory.setScaleType(ImageView.ScaleType.FIT_XY);
        viewHolder.ivCategory.setImageBitmap(categortBitmap);


        return convertView;
    }

    public void cartListCallback(cartListCallback myCallback) {
        this.myCallback = myCallback;
    }

    public interface cartListCallback {
        public void removeItem(int position);

        public void updateQty(int position, int qty);

        public void checkBoxSort(int position, int index, boolean checked);

    }

}
